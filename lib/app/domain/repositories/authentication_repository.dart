
import 'package:template/app/domain/models/user.dart';

abstract class AuthenticationRepository{
  Future<bool> get isSignedIn;
  Future<User> getUserData();
}