
class Either<L,R> {
  final L? _l;
  final R? _r;
  final bool isL;

  Either._(this._l, this._r, this.isL);

  factory Either.left(L failure){
    return Either._(failure,null,true);
  }

  factory Either.right(R value){
    return Either._(null, value, false);
  }

  T when<T>(T Function(L) left, T Function(R) right){
    if(isL){
      return left(_l as L);
    }
    else{
      return right(_r as R);
    }
  }
}