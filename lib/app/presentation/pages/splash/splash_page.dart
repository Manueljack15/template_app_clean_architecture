import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template/app/presentation/pages/splash/controller/splash_controller.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  @override
  void initState() {
    super.initState();

  }


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SplashController>(
        create: (_) => SplashController(),
      child: Scaffold(
        body: SafeArea(
          child: Container(),
        ),
      ),
    );
  }
}
