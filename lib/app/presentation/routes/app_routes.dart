import 'package:flutter/material.dart';
import 'package:template/app/presentation/pages/splash/splash_page.dart';
import 'package:template/app/presentation/routes/routes.dart';

Map<String, Widget Function(BuildContext)> get appRoutes{
  return {
    Routes.splash: (context) => const SplashPage(),
  };
}