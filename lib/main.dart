import 'dart:js';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template/app/data/providers/remote/internet.checker.dart';
import 'package:template/app/data/repositories_implementation/authentication_repository_implementation.dart';
import 'package:template/app/data/repositories_implementation/connectivity_repository_implementation.dart';
import 'package:template/app/domain/repositories/authentication_repository.dart';
import 'package:template/app/domain/repositories/connectivity_repository.dart';
import 'package:template/app/my_app.dart';


void main(){
  runApp(
    MultiProvider(
      providers: [
        Provider<ConnectivityRepository>(
            create: (_) => ConnectivityRepositoryImplementation(
                Connectivity(), InternetChecker()
            )),
        Provider<AuthenticationRepository>(create: (_) =>
            AuthenticationRepositoryImplementation()),
      ],
      child: const MyApp(),
    )
  );

}

